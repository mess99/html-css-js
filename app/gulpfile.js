'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');

gulp.task('sass', function () {
  return gulp.src('./src/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('./src/css'))
    .pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('watch', ['browser-sync', 'sass', 'concatJs'], function () {
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/javascript/*.js',['concatJs']);
    gulp.watch('src/*.html', browserSync.reload);
    // redoad rafrech la page web
    gulp.watch('src/javascript/*.js', browserSync.reload);
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./src/"
        }
    });
});

gulp.task('concatJs', function() {
  return gulp.src(['./src/javascript/libs/jquery-3.2.1.min.js', './src/javascript/*.js'])
    .pipe(concat('production.js'))
    .pipe(gulp.dest('src/js'));
});

gulp.task('compress', function() {
  gulp.src('src/js/production.js').pipe(minify({
      ext: {
         // optionnel, crée un fichier de debug dans Dist
          src: '-debug.js',
          min: '.js'
      },
      exclude: ['tasks'],
      ignoreFiles: ['.combo.js', '.js']
  })).pipe(gulp.dest('src/dist/js'))
 });



gulp.task('compressCss', function() {
  return gulp.src('src/css/main.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('minifyHtml', function() {
  return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('src/dist'));
});

gulp.task('copy', function(){
    gulp.src('./src/assets/img/*.*')
    .pipe(gulp.dest('src/dist/assets/img'));
    gulp.src('./src/fonts/**/*.*')
    .pipe(gulp.dest('src/dist/fonts'));

});

gulp.task('build', ['compress', 'compressCss', 'minifyHtml', 'copy' ])

